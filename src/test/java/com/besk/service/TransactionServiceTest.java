package com.besk.service;

import com.besk.dao.AccountRepository;
import com.besk.dao.TransactionRepository;
import com.besk.domain.Account;
import com.besk.domain.Transaction;
import com.besk.dto.WithdrawalRequest;
import com.besk.rest.enums.ResponseCode;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceTest {

    private static final long ACCOUNT_ID = 100L;
    private static final BigDecimal ACCOUNT_BALANCE = new BigDecimal(1000);

    @InjectMocks
    private TransactionService service;

    @Mock
    private AccountRepository accountRepository;
    @Mock
    private TransactionRepository transactionRepository;
    @Mock
    private WithdrawalLimitService withdrawalLimitService;

    private Account account;
    private WithdrawalRequest request;

    @Before
    public void setup() {
        account = new Account();
        account.setBalance(ACCOUNT_BALANCE);
        request = new WithdrawalRequest();
        request.setPersonalId(ACCOUNT_ID);
        request.setAmount(new BigDecimal(100));

        when(accountRepository.findByPersonalId(eq(ACCOUNT_ID))).thenReturn(account);
    }

    @Test
    public void testWithdrawMoney_accountNoFound() throws Exception {
        request.setPersonalId(ACCOUNT_ID + 1);
        String result = service.withdrawMoney(request, null);
        assertEquals(ResponseCode.ACCOUNT_NOT_FOUND.name(), result);
    }

    @Test
    public void testWithdrawMoney_insufficientBalance() throws Exception {
        request.setAmount(ACCOUNT_BALANCE.multiply(BigDecimal.TEN));
        String result = service.withdrawMoney(request, null);
        assertEquals(ResponseCode.REJECTED.name(), result);
    }

    @Test
    public void testWithdrawMoney_exceedWithdrawalLimit() throws Exception {
        when(withdrawalLimitService.canWithdrawMoney()).thenReturn(false);

        String result = service.withdrawMoney(request, null);

        assertEquals(ResponseCode.REJECTED.name(), result);
    }

    @Test
    public void testWithdrawMoney_happyPath() throws Exception {
        when(withdrawalLimitService.canWithdrawMoney()).thenReturn(true);

        BigDecimal expectedBalance = ACCOUNT_BALANCE.subtract(request.getAmount());

        String result = service.withdrawMoney(request, null);

        assertEquals(ResponseCode.SUCCESS.name(), result);
        assertEquals(expectedBalance, account.getBalance());
        verify(accountRepository, times(1)).save(account);
        verify(transactionRepository, times(1)).save((Transaction) any());
    }
}