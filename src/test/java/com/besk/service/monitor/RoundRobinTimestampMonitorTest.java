package com.besk.service.monitor;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


public class RoundRobinTimestampMonitorTest {

    private RoundRobinTimestampMonitor monitor;

    @Before
    public void setup() {
        monitor = new RoundRobinTimestampMonitor();
        monitor.init();
    }

    @Test
    public void testAcquireSlot() throws Exception {
        final int SIZE = 51;
        int ctr = 0;
        for (int i = 0; i < SIZE; i++) {
            ctr += monitor.acquireSlot() ? 1 : 0;
        }
        assertNotEquals(SIZE, ctr);
    }
}