package com.besk.dao;


import com.besk.domain.Account;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account, Long> {
    Account findByPersonalId(Long personalId);
}
