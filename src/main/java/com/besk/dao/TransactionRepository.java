package com.besk.dao;


import com.besk.domain.Transaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TransactionRepository extends CrudRepository<Transaction, Long> {
    @Query(value = "select * from Transaction t order by t.created_on desc limit ?1 offset ?2", nativeQuery = true)
    List<Transaction> findAll(long size, long first);

    @Query(value = "select * from Transaction t where t.account_personal_id = ?1 order by t.created_on desc limit ?2 offset ?3", nativeQuery = true)
    List<Transaction> findAll(long accountPersonalId, long size, long first);
}
