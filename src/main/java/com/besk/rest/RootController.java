package com.besk.rest;

import com.besk.dto.AccountOperationDto;
import com.besk.dto.CreateDepositRequest;
import com.besk.dto.WithdrawalRequest;
import com.besk.service.AccountService;
import com.besk.service.GeolocationService;
import com.besk.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController()
public class RootController {

    @Autowired
    private AccountService accountService;
    @Autowired
    private TransactionService transactionService;
    @Autowired
    private GeolocationService geolocationService;

    @RequestMapping("/deposit")
    public void createDeposit(@Valid @RequestBody CreateDepositRequest request) {
        accountService.createAccount(request);
    }

    @RequestMapping("/withdraw")
    public String withdraw(HttpServletRequest metadata, @Valid @RequestBody WithdrawalRequest request) {
        String countryCode = geolocationService.resolveCountryByIp(metadata.getRemoteAddr());
        return transactionService.withdrawMoney(request, countryCode);
    }

    @RequestMapping("/listall")
    public List<AccountOperationDto> listAll(@RequestParam(required = false) Long size, @RequestParam(required = false) Long first) {
        size = size == null ? 100 : size;
        first = first == null ? 0 : first;
        return transactionService.getAllOperations(size, first);
    }

    @RequestMapping("/list")
    public List<AccountOperationDto> list(@RequestParam Long personalId, @RequestParam(required = false) Long size, @RequestParam(required = false) Long first) {
        size = size == null ? 100 : size;
        first = first == null ? 0 : first;
        return transactionService.getAllOperationsByAccountId(personalId, size, first);
    }
}
