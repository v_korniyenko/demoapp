package com.besk.rest.enums;


public enum ResponseCode {
    SUCCESS, REJECTED, ACCOUNT_NOT_FOUND
}
