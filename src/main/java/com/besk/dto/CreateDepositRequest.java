package com.besk.dto;


import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class CreateDepositRequest {
    @NotNull @DecimalMin("0.01")
    private BigDecimal amount;
    @NotNull
    private Long personalId;
    @NotNull
    private String accountName;
    @NotNull
    private String accountSurname;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getPersonalId() {
        return personalId;
    }

    public void setPersonalId(Long personalId) {
        this.personalId = personalId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountSurname() {
        return accountSurname;
    }

    public void setAccountSurname(String accountSurname) {
        this.accountSurname = accountSurname;
    }
}
