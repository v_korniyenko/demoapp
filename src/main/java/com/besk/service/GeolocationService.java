package com.besk.service;

import com.besk.dto.IpApiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Service
public class GeolocationService {
    private static final Logger LOG = LoggerFactory.getLogger(GeolocationService.class);

    @Value("${demoapp.default-country-code}")
    private String defaultCountryCode;
    @Value("${demoapp.ip-api-endpoint}")
    private String ipApiEndpoint;

    public String resolveCountryByIp(String ip) {
        RestTemplate restTemplate = new RestTemplate();
        try {
            IpApiResponse response = restTemplate.getForObject(ipApiEndpoint + ip, IpApiResponse.class);
            if (response != null && !StringUtils.isEmpty(response.getCountryCode())) {
                return response.getCountryCode().toLowerCase();
            }
        } catch (RestClientException ex) {
            LOG.warn("Error resolving country by ip", ex);
        }
        return defaultCountryCode;
    }
}
