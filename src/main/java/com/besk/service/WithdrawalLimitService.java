package com.besk.service;

import com.besk.service.monitor.RoundRobinTimestampMonitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WithdrawalLimitService {
    @Autowired
    private RoundRobinTimestampMonitor monitor;

    public boolean canWithdrawMoney() {
        return monitor.acquireSlot();
    }
}
