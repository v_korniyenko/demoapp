package com.besk.service;


import com.besk.dao.AccountRepository;
import com.besk.domain.Account;
import com.besk.dto.CreateDepositRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    public void createAccount(CreateDepositRequest request) {
        Account account = new Account();
        account.setPersonalId(request.getPersonalId());
        account.setName(request.getAccountName());
        account.setSurname(request.getAccountSurname());
        account.setBalance(request.getAmount());
        accountRepository.save(account);
    }
}
