package com.besk.service.monitor;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.ApplicationScope;

import javax.annotation.PostConstruct;
import java.util.Date;

@ApplicationScope
@Component
public class RoundRobinTimestampMonitor {

    @Value("${demoapp.max-requests-for-period}")
    private int maxRequestsForPeriod = 50;
    @Value("${demoapp.min-timeframe-in-ms}")
    private int minTimeframe = 1000;

    private final Object lock = new Object();

    private long[] timestamp;
    private int ptr;

    @PostConstruct
    public void init() {
        timestamp = new long[maxRequestsForPeriod];
    }

    public boolean acquireSlot() {
        long now = new Date().getTime();
        synchronized (lock) {
            if (now - timestamp[ptr] >= minTimeframe) {
                timestamp[ptr++] = now;
                ptr = ptr % timestamp.length;
                return true;
            }
        }
        return false;
    }
}
