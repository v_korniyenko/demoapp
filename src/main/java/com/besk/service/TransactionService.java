package com.besk.service;

import com.besk.dao.AccountRepository;
import com.besk.dao.TransactionRepository;
import com.besk.domain.Account;
import com.besk.domain.Transaction;
import com.besk.domain.enums.TransactionType;
import com.besk.dto.AccountOperationDto;
import com.besk.dto.WithdrawalRequest;
import com.besk.rest.enums.ResponseCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TransactionService {

    private static final Logger LOG = LoggerFactory.getLogger(TransactionService.class);

    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private WithdrawalLimitService withdrawalLimitService;

    public String withdrawMoney(WithdrawalRequest request, String countryCode) {
        Account account = accountRepository.findByPersonalId(request.getPersonalId());
        if (account == null) {
            LOG.warn("account with id {} was not found", request.getPersonalId());
            return ResponseCode.ACCOUNT_NOT_FOUND.name();
        }
        if (account.getBalance().compareTo(request.getAmount()) < 0) {
            LOG.warn("insufficient balance on account id={}", account.getPersonalId());
            return ResponseCode.REJECTED.name();
        }
        if (!withdrawalLimitService.canWithdrawMoney()) {
            LOG.warn("exceeded limit for withdrawal");
            return ResponseCode.REJECTED.name();
        }

        BigDecimal newBalance = account.getBalance().subtract(request.getAmount());

        account.setBalance(newBalance);
        Transaction t = newWithdrawalTransaction(request, countryCode);
        accountRepository.save(account);
        transactionRepository.save(t);

        return ResponseCode.SUCCESS.name();
    }


    public List<AccountOperationDto> getAllOperations(long size, long first) {
        List<Transaction> list = transactionRepository.findAll(size, first);
        return toAccountOperationDtoList(list);
    }

    public List<AccountOperationDto> getAllOperationsByAccountId(long accountPersonalId, long size, long first) {
        List<Transaction> list = transactionRepository.findAll(accountPersonalId, size, first);
        return toAccountOperationDtoList(list);
    }

    private List<AccountOperationDto> toAccountOperationDtoList(List<Transaction> list) {
        return list.stream().map(this::toAccountOperation).collect(Collectors.toList());
    }

    private AccountOperationDto toAccountOperation(Transaction transaction) {
        AccountOperationDto dto = new AccountOperationDto();
        dto.setPersonalId(transaction.getAccountPersonalId());
        dto.setAmount(transaction.getAmount());
        dto.setCountryCode(transaction.getCountryCode());
        dto.setTimestamp(transaction.getCreatedOn());
        return dto;
    }

    private Transaction newWithdrawalTransaction(WithdrawalRequest request, String countryCode) {
        Transaction t = new Transaction();
        t.setAccountPersonalId(request.getPersonalId());
        t.setTransactionType(TransactionType.WITHDRAWAL);
        t.setAmount(request.getAmount());
        t.setCountryCode(countryCode);
        t.setCreatedOn(new Date());
        return t;
    }
}
